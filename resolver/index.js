export default {
  Query: {
    users: async (parent, args, { models }) => {
      return await models.User.findAll();
    },
    
    user: async (parent, { id }, { models }) => {
      return await models.User.findByPk(id);
    }
  },
  Mutation: {
    create: async (parent, { nama ,password }, { models }) => {
      return await models.User.create({
        nama,
        password
      });
    },
    cariUsers: async (parent, { nama } , { models }) => {
      return await models.User.findAll({
        where: {
          nama 
        }
      });
    },

   
    delete: async (parent, { id }, { models }) => {
      return await models.User.destroy({
        where: {
          id
        }
      });
    },
    update: async (parent, { id, nama, password }, { models }) => {
      await models.User.update(
        {
          nama,
          password
        },
        {
          where: {
            id: id
          }
        }
      );
    }
  }
};