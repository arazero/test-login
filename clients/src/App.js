import React,{ Component } from 'react';


// import logo from './logo.svg';
import './App.css';
import Data from './components';
import {BrowserRouter, Link, Switch, Route } from 'react-router-dom';
 import Login from './components/pages/Login';
import Admin from './components/pages/Admin';
import Logout from './components/pages/Logout';
 import Register from './components/pages/Register';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
const client = new ApolloClient({
  uri: 'http://localhost:8999/graphql'
});


function App() {
  return (
    
    <ApolloProvider client={client}>
    <div className="App">
      {/* <Data /> */}
    <BrowserRouter>
    <Switch>
     
      <Route  exact path="/" component={ Login } />
      <Route  path="/admin" component={ Admin } />
      <Route  path="/register" component={ Register} />
      <Route  path="/logout" component={ Logout } />
    </Switch>
    </BrowserRouter>
     
   </div>
   </ApolloProvider>

    
  );
}

export default App;
