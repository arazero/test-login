import React, { Component } from 'react'
import { graphql,Mutation } from 'react-apollo';
import { addUser } from '../query/query'
import { Redirect , Link } from 'react-router-dom'
import { useMutation } from '@apollo/react-hooks';
import { sign } from 'crypto';
import { Button, Form, Grid, Header, Image, Message, Segment,Confirm } from 'semantic-ui-react'
import logo from '../../logo.svg';
class Register extends Component {
  state = { open: false, redirect: false  }

  show = () => this.setState({ open: true })
  handleConfirm = () => this.setState({ open: false , redirect: true })
 
 
  handleCancel = () => this.setState({ open: false, nama:"", password: "" })
  constructor(props){
    super(props)
    let logged = false;
    this.state ={
        nama: '',
        password: '',
        logged
    }
    this.onChange = this.onChange.bind(this)
   

  }

  onChange(e){
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  render() {
    if(this.state.redirect){
      return <Redirect to="/" />;
  }
    return (
    
      // console.log(this.props.data),
     
      // <div className="App">
      //   <h1> Register</h1>

      //   <Mutation mutation={addUser}>
      //     {(adaUser,{data})=> (
      //   <form onSubmit= {e => {
      //     e.preventDefault();
      //     const nama = this.state
      //     adaUser({ variables: { 
      //       nama : this.state.nama,
      //      password: this.state.password 
      //       }
      //     });
      //     if(window.confrim(`nama : ${nama} sudah terdaftar`)){
      //       return <Redirect to="/"/>

      //     }
      //   }}>
      //     <input type='text' placeholder='nama' name='nama' value={this.state.nama} onChange={this.onChange} />
      //     <br/>
      //     <input type='password' placeholder='password' name='password' value={this.state.password} onChange={this.onChange}/>
      //     <br/>
      //     <input type='submit' />
      //     <br/>
      //   </form>
      //     )}
      //   </Mutation>
      //   <Link to="/">Login</Link>
      // </div>

      <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h1' color='blue' textAlign='center'>
          <Image src={logo} /> Register Now
        </Header>
        <Mutation mutation={addUser}>
          {(adaUser,{data})=> (
        <Form size='large' onSubmit= {e => {
          e.preventDefault();
          this.show();
          // const nama = this.state
          // adaUser({ variables: { 
          //   nama : this.state.nama,
          //  password: this.state.password 
          //   }
          // });
          
         
          
          
          
        }}>
          <Segment stacked>
            <Form.Input  name='nama' value={this.state.nama} onChange={this.onChange} fluid icon='user' iconPosition='left' placeholder='Nama' />
            <Form.Input
              fluid
              icon='lock'
              iconPosition='left'
              placeholder='Password'
              type='password'
              name='password' value={this.state.password} onChange={this.onChange}
            />

            <Button color='blue' fluid size='large'>
              Save
            </Button>
            <Confirm
          open={this.state.open}
          header ={`nama : ${this.state.nama}  telah terdaftar `}
          content='Lanjut Redirect Login Page?'
          onCancel={this.handleCancel }
          onConfirm={this.handleConfirm } 
        />
          </Segment>
        </Form>
         )}
         </Mutation>
        <Message>
          Sudah Punya akun? <a href='/'>Login</a>
        </Message>
      </Grid.Column>
      </Grid>
    )
  }
}
export default graphql(addUser)(Register);