import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Logout extends Component {
  constructor(props){
    super(props)
    localStorage.removeItem('token')
    }
  render() {
    return (
      <div>
        <h3> Telah log out silahkan login kembali</h3>
        <Link to="/">Login</Link>
      </div>
    )
  }
}