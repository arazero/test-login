import React, { Component } from 'react'
import { Link,Redirect } from 'react-router-dom'
import { Button, Checkbox, Icon, Table } from 'semantic-ui-react'
import DataUser from './DataUser';

class Admin extends Component {
    constructor(props){
      super(props)
      const token = localStorage.getItem('token')
      let logged = true
      if(token == null){
        logged =  false;
      }
      
      this.state = {
        logged
      }

    }

  render() {
    if(this.state.logged === false){
      return <Redirect to="/" />
    }
   
    
    return (
    <div>
    <Table celled compact definition>
    <Table.Header fullWidth>
      <Table.Row>
        <Table.HeaderCell />
        <Table.HeaderCell>id</Table.HeaderCell>
        <Table.HeaderCell>Nama</Table.HeaderCell>
        <Table.HeaderCell>Password</Table.HeaderCell>
      </Table.Row>
    </Table.Header>
    <Table.Body>
      <DataUser />
      </Table.Body>
    <Table.Footer fullWidth>
      <Table.Row>
        <Table.HeaderCell />
        <Table.HeaderCell colSpan='2'>
          <Button
            floated='right'
            icon
            labelPosition='left'
            primary
            size='small'
          >
            <Icon name='user' /> Add User
          </Button>
          <Button
            floated='right'
            icon
            labelPosition='left'
            color='red'
            size='small'
            href="/logout"
          >
            <Icon name='sign out alternate' /> Log-out
          </Button>
          <Button size='small'>Approve</Button>
          <Button disabled size='small'>
            Approve All
          </Button>
        </Table.HeaderCell>
      </Table.Row>
    </Table.Footer>
    </Table>
    </div>
      
    )
  }
}

export default Admin;