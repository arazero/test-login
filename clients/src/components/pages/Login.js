import React, { Component } from 'react'
import { Redirect , Link } from 'react-router-dom'
import { graphql } from 'react-apollo';
import { getAllUsers } from '../query/query'
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react'
import logo from '../../logo.svg';
class Login extends Component {
  constructor(props){
    super(props)
    let logged = false;
    this.state ={
        nama: '',
        password: '',
        logged
    }
    this.onChange = this.onChange.bind(this)
    this.submitForm = this.submitForm.bind(this)

  }


  submitForm(e){
    e.preventDefault()
     const { nama,password } = this.state
     let dataUsers = this.props.data.users
     var adaUsers = dataUsers.find(dataUsers => dataUsers.nama === nama && dataUsers.password === password);
        if(adaUsers){
        localStorage.setItem('token','aidwadioamskdnqondwuvj');
        this.setState({
          logged: true
        })
      }else{
      
      return;
      }
  }
 

  onChange(e){
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  render() {

    if(this.state.logged){
      return <Redirect to='/admin'/>
    }
    return (
    // console.table(this.props.data.users),
     
    <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
    <Grid.Column style={{ maxWidth: 450 }}>
      <Header as='h2' color='teal' textAlign='center'>
        <Image src={logo} /> Log-in Your account
      </Header>
      <Form size='large' onSubmit={this.submitForm}>
        <Segment stacked>
          <Form.Input  name='nama' value={this.state.nama} onChange={this.onChange} fluid icon='user' iconPosition='left' placeholder='Nama' />
          <Form.Input
            fluid
            icon='lock'
            iconPosition='left'
            placeholder='Password'
            type='password'
            name='password' value={this.state.password} onChange={this.onChange}
          />

          <Button color='teal' fluid size='large'>
            Login
          </Button>
        </Segment>
      </Form>
      <Message>
        New to us? <a href='/Register'>Sign Up</a>
      </Message>
    </Grid.Column>
    </Grid>

     
   
    )
  }
}
// strucktrur export default graphql(query)(component);
export default graphql(getAllUsers)(Login);