import React, { Component } from 'react'
import { useMutation, useQuery } from '@apollo/react-hooks';
import { Button, Checkbox, Icon, Table } from 'semantic-ui-react'
import { getAllUsers } from '../query/query';
import { graphql } from 'react-apollo';
 function DataUser (){
  
    const { loading, error, data } = useQuery(getAllUsers);
    if (loading) return <p>Loading...</p>;
    if (error) return <p>error :( </p> ;
  
    return data.users.map(({ id, nama,password },index) => (
      <Table.Row key={index}>
      <Table.Cell collapsing>
        <Checkbox slider />
      </Table.Cell>
      <Table.Cell>{ id }</Table.Cell>
      <Table.Cell>{ nama }</Table.Cell>
      <Table.Cell>{ password }</Table.Cell>
    </Table.Row>

    ));
  }

export default graphql(getAllUsers)(DataUser);