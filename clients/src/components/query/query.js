// import { gql } from 'apollo-boost';
 import  gql from 'graphql-tag';

const addUser = gql`
  mutation($nama: String!, $password: String!){
    create(nama: $nama , password: $password){
    nama,
    password
  }
}

`;

const getAllUsers = gql`
  {
    users{
    id,
    nama,
    password
  }
}
`;

// function

export { addUser , getAllUsers } ;