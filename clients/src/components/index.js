import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { getAllUsers } from './query/query'
// import * as graphql  from 'react-apollo'
import { graphql } from 'react-apollo';



function Data() {
  const { loading, error, data } = useQuery(getAllUsers);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>error :( </p> ;

  return data.users.map(({ nama,password },index) => (
    <div key={index}>
      <p>
         {nama}, { password }
      </p>
    </div>
  ));

}
export default graphql(getAllUsers)(Data);