
const db = require('../config/database');
// const models = {
//   Note : db.import("./notes")
// };

const models = {
  User : db.import("./users")
};

Object.keys(models).forEach((key) => {
  if ("associate" in models[key]) {
    models[key].associate(models);
  }
});

export { db };
export default models; 