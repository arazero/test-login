const user = ( db, DataTypes) => {
  const User= db.define('user', {
    // attributes
    nama: {
      type: DataTypes.STRING
    },
    password: {
      type: DataTypes.STRING
    },
  },{
    schema: 'murid',
  });
  return User;
};


export default user;