const note = ( db, DataTypes) => {
  const Note = db.define('note', {
    // attributes
    text: {
      type: DataTypes.STRING
    },
  },{
    schema: 'murid',
  });
  return Note ;
};


export default note;