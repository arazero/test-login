const express = require('express');
const { ApolloServer, gql } = require('apollo-server-express');
import { importSchema } from 'graphql-import';
import { from } from 'apollo-link';
import models, { db } from './models';

/* Database */

import resolvers  from './resolver';
const typeDefs =  importSchema('./schema/users.graphql');

const server = new ApolloServer({ 
  typeDefs : typeDefs, 
  resolvers : resolvers,
  context : {
    models,
  }
});

const app = express();
server.applyMiddleware({ app });



db.sync().then(async () => {
  app.listen({
      port: 8999// you could change this to process.env.PORT if you wish
  }, () => {
      console.log('Apollo Server on http://localhost:8999/graphql');
  });
})